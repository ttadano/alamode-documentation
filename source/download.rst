Download
========

The latest stable version of the package ALAMODE can be downloaded from `this link <http://white.phys.s.u-tokyo.ac.jp/~tadano/alamode/alamode-0.9.0.tar.gz>`_.

We will also provide the Git repository of ALAMODE shortly.


